#include "test.hpp"

extern TIM_HandleTypeDef htim8;
extern UART_HandleTypeDef huart1;
extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc2;

volatile CurrentData currentdata;
volatile EncoderData encoderdata;

SbyIsMySon::SbyIsMySon()
{}

SbyIsMySon::~SbyIsMySon()
{}
	
float SbyIsMySon::VariableIntegralCoefficient(float error,float absmax,float absmin)
{
    float factor=0.0;
 
    if(fabs(error)<=absmin){
        factor=1.0;
    }
    else if(fabs(error)>absmax){
        factor=0.0;
    }
    else{
        factor=(absmax-fabs(error))/(absmax-absmin);
    }
    return factor;
}

float SbyIsMySon::PIDRegulator(float setpoint,float currpoint)
{
    float thisError;
    float result;
    float factor;
	
	static float last_point;
	
	// if(currpoint - last_point > 700) return 500.0;
	
	sbypiddata.setpoint = setpoint;

    thisError=sbypiddata.setpoint-currpoint; 
    result=sbypiddata.result;
	
	factor=VariableIntegralCoefficient(thisError,sbypiddata.errorabsmax,sbypiddata.errorabsmin);
	sbypiddata.integralValue += factor*thisError;

	sbypiddata.derivative = sbypiddata.kd*(1-sbypiddata.alpha)*(thisError-sbypiddata.lasterror +sbypiddata.alpha*sbypiddata. derivative);
	result=sbypiddata.kp*thisError+sbypiddata.ki*sbypiddata. integralValue +sbypiddata. derivative;
    
    if(result>=sbypiddata.maximum){
        result=sbypiddata.maximum;
    }

    if(result<=sbypiddata.minimum){
        result=sbypiddata.minimum;
    }

    sbypiddata.preerror=sbypiddata.lasterror;  
    sbypiddata.lasterror=thisError;
    sbypiddata.result=result;
		
	// printf("%d,%d,%d\n",(int16_t)currpoint,(int16_t)result/10,(int16_t)setpoint);
	
	last_point = currpoint;

	return sbypiddata.result;
}


void SbyIsMySon::MotorControl(int16_t ch,int16_t pwm)
{
    switch (ch)
    {
    case 1:
        if(pwm>=0){
            __HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_1, pwm);
            __HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_2, 0);
        }else{
           __HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_1, 0);
           __HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_2, pwm); 
        }
        break;
    case 2:
        if(pwm>=0){
            __HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_3, pwm);
            __HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_4, 0);
        }else{
           __HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_3, 0);
           __HAL_TIM_SET_COMPARE(&htim8, TIM_CHANNEL_4, pwm); 
        }
        break;
    default:
        break;
    }
}

int SbyIsMySon::GetCurrentControlState(float pos)
{
    if(pos > (encoderdata.PositionA + encoderdata.PositionB)/2) 
        return 1;  
    if(pos <= (encoderdata.PositionA + encoderdata.PositionB)/2)
        return 2; 
}

bool SbyIsMySon::ObjectMoveRightInCurrentMode(float pos)
{
    static int PWMA,PWMB;
    if(fabs(currentdata.A - (sbycurrentsettingdata.CurrentBasicA
    +sbycurrentsettingdata.CurrentForceA)) < sbycurrentsettingdata.CurrentErrorA){
        PWMA =  basicconfigdata.BasicPwmData;
        PWMB =  (int)(5.5*PIDRegulator(sbycurrentsettingdata.CurrentForceA,currentdata.A));
    }
    else{  
        PWMA = 0;
        PWMB = (int)(5.5*PIDRegulator(sbycurrentsettingdata.CurrentForceA,currentdata.A));
    }
    
    MotorControl(1,PWMA);
    MotorControl(2,PWMB);
    
    return true;
} 

bool SbyIsMySon::ObjectMoveLeftInCurrentMode(float pos)
{
    static int PWMA,PWMB;
    if(fabs(currentdata.B - (sbycurrentsettingdata.CurrentBasicB
    +sbycurrentsettingdata.CurrentForceB)) < sbycurrentsettingdata.CurrentErrorB){ 
        PWMA =  basicconfigdata.BasicPwmData;
        PWMB =  (int)(5.5*PIDRegulator(sbycurrentsettingdata.CurrentForceB,currentdata.B));
    }
    else{ 
        PWMA = 0;  
        PWMB = (int)(5.5*PIDRegulator(sbycurrentsettingdata.CurrentForceB,currentdata.B));
    }
    
    MotorControl(1,PWMB);
    MotorControl(2,PWMA);
    
    return true;    
}

bool SbyIsMySon::ObjectMoveRightInEncoderMode(float pos)
{
    static int PWMA,PWMB;
    if(fabs(encoderdata.PositionA - encoderdata.PositionB) < 
            encodersettingdata.PositionError+encodersettingdata.MinimumSize){
        PWMA =  basicconfigdata.BasicPwmData;
        PWMB =  (int)(5.5*PIDRegulator(encodersettingdata.PositionError,encoderdata.PositionA - encoderdata.PositionB));
    }
    else{  
        PWMA = 0;
        PWMB =  (int)(5.5*PIDRegulator(encodersettingdata.PositionError,encoderdata.PositionA - encoderdata.PositionB));
    }
    
    MotorControl(1,PWMA);
    MotorControl(2,PWMB);
    
    return true;
} 

bool SbyIsMySon::ObjectMoveLeftInEncoderMode(float pos)
{
    static int PWMA,PWMB;
    if(fabs(encoderdata.PositionA - encoderdata.PositionB) < 										
            encodersettingdata.PositionError+encodersettingdata.MinimumSize){
        PWMB =  basicconfigdata.BasicPwmData;
        PWMA =  (int)(5.5*PIDRegulator(encodersettingdata.PositionError,encoderdata.PositionA - encoderdata.PositionB));
    }
    else{  
        PWMB = 0;
        PWMA =  (int)(5.5*PIDRegulator(encodersettingdata.PositionError,encoderdata.PositionA - encoderdata.PositionB));
    }
    
    MotorControl(1,PWMA);
    MotorControl(2,PWMB);
    
    return true;
} 

void SbyIsMySon::UpdateConfig(int PIDtype,SbyPID sbypid,SbyCurrentSetting sbycurrentsetting,EncoderSetting encodersetting,BasicConfig basicconfig)
{
    switch(PIDtype)
    {
        case currentPID:
            currentpiddata = sbypid;
            break;
        case speedPID:
            speedpiddata_A = sbypid;
            break;
        case positionPID:
            speedpiddata_B = sbypid;
    }
    sbycurrentsettingdata = sbycurrentsetting;
    encodersettingdata = encodersetting;
    basicconfigdata = basicconfig;
}

bool SbyIsMySon::CurrentControl(int state,float pos)
{
    switch (state)
    {
    case 1:
        ObjectMoveRightInCurrentMode(pos);
        break;
    case 2:
        ObjectMoveLeftInCurrentMode(pos);
        break;
    default:
        break;
    }
    return true;
}

bool SbyIsMySon::PositionControl(int state,float pos)
{
    switch (state)
    {
    case 1:
        ObjectMoveRightInEncoderMode(pos);
        break;
    case 2:
        ObjectMoveLeftInEncoderMode(pos);
        break;
    default:
        break;
    }
    return true;
}

float SbyIsMySon::CurrentAndPositionControl(float pos)
{
    if(encoderdata.PositionB - encoderdata.PositionA < encodersettingdata.MinimumSize)
        return -1;
    if(CurrentControl(GetCurrentControlState(pos),pos))
        return fabs(encoderdata.PositionB - encoderdata.PositionA);
    else   
        return -2;
}

float SbyIsMySon::PositionWithoutCurrentControl(float pos)
{
    if(encoderdata.PositionB - encoderdata.PositionA < encodersettingdata.MinimumSize)
        return -1;
    if(PositionControl(GetCurrentControlState(pos),pos))
        return fabs(encoderdata.PositionB - encoderdata.PositionA);
    else   
        return -2;
}

float SbyIsMySon::PulseToMicrometer(uint32_t pulse,int T)
{
    if(T == 0) return (float)pulse * 0.5134;
    if(T == 1) return (float)pulse * 0.6425;
}

uint32_t SbyIsMySon::MicrometerToPulse(float mm,int T)
{
    if(T == 0) return (uint32_t)(mm/0.005134);
    if(T == 1) return (uint32_t)(mm/0.006425);
}

void SbyIsMySon::TestCurrent(float current)
{
    static int PWMA,PWMB;
	
    PWMB = 0;
    PWMA =  (int)PIDRegulator(current,currentdata.A);
    MotorControl(1,PWMA);
	
}

void SbyIsMySon::TestSpeed(float speed)
{
    static int PWMA,PWMB;

    PWMB = 0;
    PWMA =  (int)PIDRegulator(speed,fabs(encoderdata.SpeedA));
    MotorControl(1,PWMA);

}

bool SbyIsMySon::GoToPosition(float mmA,float mmB)
{
    int PWMA,PWMB,T;
		static int32_t pulsedata_a,pulsedata_b;
	
		pulsedata_a = encoderdata.PositionWithoutTransformA;
		pulsedata_b = encoderdata.PositionWithoutTransformB;
	
		if(pulsedata_a<=0) pulsedata_a=0;
		if(pulsedata_b<=0) pulsedata_b=0;
	
		if(mmA <= encodersettingdata.PositionError/2) mmA = encodersettingdata.PositionError/2;
		if(mmB <= encodersettingdata.PositionError/2) mmB = encodersettingdata.PositionError/2;
	
		T = 0;

       sbypiddata = speedpiddata_A;
	
        if(MicrometerToPulse(mmA + encodersettingdata.PositionError/2,0) <= pulsedata_a)
        {
            PWMA =  -(uint16_t)PIDRegulator(45,fabs(encoderdata.SpeedA));
        }
        else if(MicrometerToPulse(mmA - encodersettingdata.PositionError/2,0) >= pulsedata_a)
        {
            PWMA =  (uint16_t)PIDRegulator(45,fabs(encoderdata.SpeedA));
        }
		else
		{
			T++;
			PWMA = 0;
		}

        sbypiddata = speedpiddata_B;
	
        if(MicrometerToPulse(mmB + encodersettingdata.PositionError/2,1) <= pulsedata_b)
        {
            PWMB = (int16_t)PIDRegulator((uint16_t)((float)45*1.0),fabs(encoderdata.SpeedB));
        }
        else if(MicrometerToPulse(mmB - encodersettingdata.PositionError/2,1) >= pulsedata_b)
        {
            PWMB = -(int16_t)PIDRegulator((uint16_t)((float)45*0.65),fabs(encoderdata.SpeedB));
        }
		else
		{
			T++;
			PWMB = 0;
		}
	
		printf("pos:%d %d %d\r\n",encoderdata.PositionWithoutTransformA,MicrometerToPulse(mmA,0),PWMA);		
		printf("pos:%d %d %d\r\n\r\n",encoderdata.PositionWithoutTransformB,MicrometerToPulse(mmB,1),PWMB);
		
//		printf("pos:%lfmm %lfmm\r\n",encoderdata.PositionA,encoderdata.PositionB);
		
    MotorControl(1,PWMA);
		MotorControl(2,PWMB);

        HAL_Delay(10);
		
		if(T == 2)  return true;
		else return false;
}

/* 
 * 执行XY坐标
 * 输入参数：
 *     x1、y1 : 起点坐标
 *     x2、y2 : 终点坐标
 */
void SbyIsMySon::ExcuteInXY(int x,int y)
{
    SbyXY ab;
    ab = InverseKinematics(x,y);
	while(!GoToPosition((int16_t)ab.CACL_A,(int16_t)ab.CACL_B));
}

/* 
 * 直线插补算法
 * 输入参数：
 *     x1、y1 : 起点坐标
 *     x2、y2 : 终点坐标
 */
void SbyIsMySon::LinearInterpolator(int x1,int y1,int x2,int y2)
{
    int dx,dy,e;
    dx=x2-x1; 
    dy=y2-y1;
    if(dx>=0)
    {
        if(dy >= 0) // dy>=0
        {
            if(dx>=dy) // 1/8 octant
            {
                e=dy-dx/2;
                while(x1<=x2)
                {
                    ExcuteInXY(x1,y1);
                    if(e>0){y1+=1;e-=dx;}   
                    x1+=1;
                    e+=dy;
                }
            }
            else        // 2/8 octant
            {
                e=dx-dy/2;
                while(y1<=y2)
                {
                    ExcuteInXY(x1,y1);
                    if(e>0){x1+=1;e-=dy;}   
                    y1+=1;
                    e+=dx;
                }
            }
        }
        else           // dy<0
        {
            dy=-dy;   // dy=abs(dy)


            if(dx>=dy) // 8/8 octant
            {
                e=dy-dx/2;
                while(x1<=x2)
                {
                    ExcuteInXY(x1,y1);
                    if(e>0){y1-=1;e-=dx;}   
                    x1+=1;
                    e+=dy;
                }
            }
            else        // 7/8 octant
            {
                e=dx-dy/2;
                while(y1>=y2)
                {
                    ExcuteInXY(x1,y1);
                    if(e>0){x1+=1;e-=dy;}   
                    y1-=1;
                    e+=dx;
                }
            }
        }   
    }
    else //dx<0
    {
        dx=-dx;     //dx=abs(dx)
        if(dy >= 0) // dy>=0
        {
            if(dx>=dy) // 4/8 octant
            {
                e=dy-dx/2;
                while(x1>=x2)
                {
                    ExcuteInXY(x1,y1);
                    if(e>0){y1+=1;e-=dx;}   
                    x1-=1;
                    e+=dy;
                }
            }
            else        // 3/8 octant
            {
                e=dx-dy/2;
                while(y1<=y2)
                {
                    ExcuteInXY(x1,y1);
                    if(e>0){x1-=1;e-=dy;}   
                    y1+=1;
                    e+=dx;
                }
            }
        }
        else           // dy<0
        {
            dy=-dy;   // dy=abs(dy)
            if(dx>=dy) // 5/8 octant
            {
                e=dy-dx/2;
                while(x1>=x2)
                {
                    ExcuteInXY(x1,y1);
                    if(e>0){y1-=1;e-=dx;}   
                    x1-=1;
                    e+=dy;
                }
            }
            else        // 6/8 octant
            {
                e=dx-dy/2;
                while(y1>=y2)
                {
                    ExcuteInXY(x1,y1);
                    if(e>0){x1-=1;e-=dy;}   
                    y1-=1;
                    e+=dx;
                }
            }
        }   
    }
}

//ax^2+bx+c=0,返回较小方程的根
SbyXY SbyIsMySon::BinaryEquationSolver(float a,float b,float c)
{
	SbyXY solu;
	float root = pow(b,2)-4*a*c;
	solu.success = true;
	if(root>=0 || a!=0)
	{
		if(root == 0)
		{
			solu.DeltaX = -b/2*a;
			goto end;
		}
		float x1 = (-b+sqrt(root))/(2*a); 
		float x2 = (-b-sqrt(root))/(2*a); 

		if(x1>x2)
			solu.DeltaX = x2;
		else
			solu.DeltaX = x1;

	}
	else 
		solu.success = false;
	end:return solu;
}

SbyXY SbyIsMySon::InverseKinematics(float x,float y)
{
	SbyXY solu = BinaryEquationSolver(1,-2*x,pow(x,2)+pow(y,2)- pow((float)L1,2));

	if(solu.success)
	{
		x -= solu.DeltaX;
		float a1 = atan(y/x);
		float sina2 = (H-L1*sin(a1))/L2;
		float cosa2 = sqrt(1-pow(sina2,2));
		float ax = L2*cosa2;

		solu.CACL_A = solu.DeltaX + (x-ax);
		solu.CACL_B = solu.DeltaX;
	}
	return solu;
	
}

void SbyIsMySon::TestKinematics(void)
{
    LinearInterpolator(150,20,150,90);
    LinearInterpolator(150,90,200,90);
    LinearInterpolator(200,90,200,20);
    LinearInterpolator(200,20,250,20);
    LinearInterpolator(250,20,250,90);
    LinearInterpolator(250,90,300,90);
    LinearInterpolator(300,90,300,20);
}

void SbyIsMySon::Function1(void)
{
		while(!GoToPosition(0,100));
	
		while(!GoToPosition(80,100));
	
		while(!GoToPosition(200,220));
	
		while(!GoToPosition(180,220));
		
}

void SbyIsMySon::Function2(void)
{
		while(!GoToPosition(0,100));
	
		while(!GoToPosition(80,100));
	
		while(!GoToPosition(160,180));
	
		while(!GoToPosition(80,100));
	
		while(!GoToPosition(160,180));

		while(!GoToPosition(80,100));
	
		while(!GoToPosition(160,180));	
	
		while(!GoToPosition(80,100));
	
		while(!GoToPosition(50,100));
}

void SbyIsMySon::Function3(void)
{
		while(!GoToPosition(0,100));
	
		while(!GoToPosition(120,100));
	
		while(!GoToPosition(220,200));
	
		while(!GoToPosition(200,200));
		
}

void SbyIsMySon::Excute(void)
{
    currentdata.A = (float)HAL_ADC_GetValue(&hadc1);

    currentdata.B = (float)HAL_ADC_GetValue(&hadc2);

	if(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_0))
		MotorControl(1,-1000);
	else
		MotorControl(1,0);
	if(HAL_GPIO_ReadPin(GPIOE,GPIO_PIN_4))
		MotorControl(2,1000);
	else
		MotorControl(2,0);
			
	if(!HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_3))
		Function1();
	if(!HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_4))
		Function2();
	if(!HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_5))
		Function3();
	if(!HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_6))
		TestKinematics();

}
