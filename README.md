# (云编程)2020吉林省电子设计大赛程序模板(A题)

### 实现视频
[传送链接](https://b23.tv/vjkKcr)
### 新特性

- 加入了硬件采集接口。
- 更新了比较牛逼的算法:[双联杆型画图XY机械臂算法和思路](https://gitee.com/harryzhangabc/harry-xy-robot)

### 目的

- 本程序仅仅是出于干饭目的和为了练习数据结构和C++然后花了点时间练习下编程(主要是手痒)。  
- 本程序基于C++编程，有能力的同学可以在上面继续编写，如果你只会C建议你新建个.c文件然后再引用C++的类进行赋值操作。

### 实现原理(仅描述基础部分 扩展部分自己想)

##### 向前运行（***采用丝杆+直流减速电机，不转的时候用手推不动***）:   
1.先测量电机1、2空转时的电流。   
2.电机1正转，给一个恒定的PWM，电机2不转。   
3.检测电机1的电流，到达一定阈值（表示一定力度）时，记录下当时电流为△i。     
4.使能电机2旋转，通过pid控制电机2，使电机1电流一直在△i左右。直线运动。   
##### 如何定位(***本程序默认采用编码器***):   
1.（无编码器）通过记录恒定输出电机、电流大小所对应的位移关系通过积分过得当前笔的位置。   
2.（有编码器）直接读取编码器就行了。      

### 状态

- 代码一定会有Bug，并且不再维护。慎用。  
- *我个人感觉我的写的已经非常标准了，实现+函数命名+注释+变量命名已经非常详细了，代码风格是仿照google写的，就算有些地方不注释也能看出来哪个是干什么的吧，难道不是吗QAQ.*

### 题目
![第一页](https://gitee.com/harryzhangabc/ji-lin-province-electronic-design-pattern/raw/master/pic/3.jpg)
![第二页](https://gitee.com/harryzhangabc/ji-lin-province-electronic-design-pattern/raw/master/pic/4.jpg)

### 硬件接线(STM32F103ZET6)
![硬件接线1](https://gitee.com/harryzhangabc/ji-lin-province-electronic-design-pattern/raw/master/pic/2.png)
![硬件接线2](https://gitee.com/harryzhangabc/ji-lin-province-electronic-design-pattern/raw/master/pic/2-1.png)

***电机线改到PC6/PC7/PC8/PC9***

### 编译状态
![编译状态](https://gitee.com/harryzhangabc/ji-lin-province-electronic-design-pattern/raw/master/pic/1.png)




